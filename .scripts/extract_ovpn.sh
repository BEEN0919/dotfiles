#!/bin/sh
# OpenVPN Tomato extraction tool

if [ ! -f "$1" ]; then
    echo "Input file not valid."
    exit 1
fi

printf "%s" "Enter connection name: (A-Z, 0-9, no spaces): "
read connection
printf "%s" "Enter Client ID or Username (from Tomato web interface): "
read clientid

file="$1"

mkdir -p "$HOME"/.cert/"$connection"/"$clientid"
tar -xf "$file" --directory /"$HOME"/.cert/"$connection"/"$clientid"
rm -r "$file"
restorecon -R "$HOME"/.cert
echo "Done! Now go into settings and configure your VPN connection."
