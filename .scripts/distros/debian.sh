#!/bin/bash
choose() { \
	
	cd /home/"$USER"
	mkdir .programs
	clear
	
	cmd=(dialog --separate-output --checklist "Select options:" 22 76 16)
	options=(1 "Install recommended APT packages" off    # any option can be set to default to "on"
			2 "Install and configure VSCode" off
			3 "Install packages for the Thinkpad XX95 series (AMD)" off
			4 "Configure the GNOME3 desktop environment" off)
	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
	clear
	for choice in $choices
	do
		case $choice in
			1)
				wget -O apt.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/apt.sh
				bash apt.sh
				rm -r apt.sh
				;;
			2)
				wget -O code.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/code.sh
				bash codium.sh
				rm -r codium.sh
				;;
			3)
				wget -O thinkpad.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/thinkpad.sh
				bash thinkpad.sh
				rm -r thinkpad.sh
				;;
			4) 
				wget -O gnome.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/gnome.sh
				bash gnome.sh 
				rm -r gnome.sh
				;;
		esac
	done
	clear
}


# Things that are executed (in order)
choose
clear
