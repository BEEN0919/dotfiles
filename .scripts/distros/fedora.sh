#!/bin/sh
# autoinstall script for rhel 8 workstation

# check if root and on rhel8
if [ "$USER" = "root" ] ; then
	printf "%s\n" "Please don't run as root"
	exit 1
fi

if hostnamectl | grep "Fedora 34" >/dev/null 2>&1 ; then

cd /home/"$USER"
mkdir .programs
clear

cmd=(dialog --separate-output --checklist "Select options:" 22 76 16)
options=(1 "Install recommended DNF packages" on    # any option can be set to default to "on"
		2 "Install recommended Flatpak packages" on
		3 "Install MultiMC Launcher" off)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
	case $choice in
		1)
			wget -O dnf.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/dnf.sh
			bash dnf.sh
			rm -r dnf.sh
			;;
		2)
			wget -O flatpak.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/flatpak.sh
			bash flatpak.sh
			rm -r flatpak.sh
			;;
		2)
			wget -O multimc.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/programs/multimc.sh
			bash multimc.sh
			rm -r multimc.sh
		;;
	esac
done
clear


# appimages
rm "$HOME"/.local/share/applications/appimage*
mkdir -pv ~/Applications
cd ~/Applications

wget https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher-lite-2.2.0-travis995-0f91801-x86_64.AppImage -O appimagelauncher-lite.appimage
chmod +x appimagelauncher-lite.appimage
./appimagelauncher-lite.appimage install

#trim the fat
systemctl --user disable abrt
systemctl --user mask packagekit
systemctl --user mask evolution-addressbook-factory evolution-calendar-factory evolution-source-registry tracker-miner-apps tracker-miner-fs tracker-store

else
echo "Please install on a Fedora 34 system."
fi
