#!/bin/bash
function pause(){
   read -p "$*"
}

exitmsg() { \
	# clear
    echo "done = good;"
    echo "The installer has finished."
    echo
	}

installprograms() { \
	sudo dpkg --add-architecture i386
	sudo apt install -y yubikey-manager gparted binutils unzip mlocate neofetch libreoffice htop gimp gnome-tweaks exfat-utils exfat-fuse openjdk-8-jre openjdk-11-jre openjdk-8-jdk openjdk-11-jdk 
	sudo apt remove firefox
	sudo apt -y autoremove
	sudo apt install --fix-broken 
	cd /home/"$USER"
	mkdir .programs
	clear
	# prompt to install ungoogled chromium
	read -e -p "Would you like to install Ungoogled Chromium? (y/n)" choice
	[[ "$choice" == [Yy]* ]] && echo "Installing Ungoogled Chromium..." && installunchromium && read -e -p "Would you like to install the Widevine DRM Module? You need this to view Netflix, Disney+, or Hulu. (y/n)" choice
	[[ "$choice" == [Yy]* ]] && echo "Installing Widevine DRM Module..." && installwidevine
	# prompt to install vscodium & frc tools
	read -e -p "Would you like to install VSCodium? (y/n)" choice
	[[ "$choice" == [Yy]* ]] && echo "Installing VSCodium..." && installvscodium && read -e -p "Would you like to install WPILib for FRC? (y/n)" choice
	[[ "$choice" == [Yy]* ]] && echo "Installing FRC Tools..." && installwpilib
	# clear
	read -e -p "Would you like to install VMWare Workstation 15.5.1? (y/n)" choice
	[[ "$choice" == [Yy]* ]] && echo "Installing VMware Workstation 15.5.1... This may take a bit." && installvmware
	clear
}

installunchromium(){ \
	mkdir -p /home/$USER/.programs/ungoogled-chromium-79.0.3945.117
	cd /home/$USER/.programs/ungoogled-chromium-79.0.3945.117
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium-common_79.0.3945.117-1.eoan1_amd64.deb
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium-driver_79.0.3945.117-1.eoan1_amd64.deb
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium-l10n_79.0.3945.117-1.eoan1_all.deb
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium-sandbox_79.0.3945.117-1.eoan1_amd64.deb
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium-shell_79.0.3945.117-1.eoan1_amd64.deb
	wget https://github.com/riyad/ungoogled-chromium-binaries/releases/download/79.0.3945.117-1.eoan1/ungoogled-chromium_79.0.3945.117-1.eoan1_amd64.deb
	sudo dpkg -i ungoogled-chromium_*.deb ungoogled-chromium-common_*.deb
	sudo rm -rf ungoogled-chromium_*.deb ungoogled-chromium-common_*.deb
	sudo apt install --fix-broken
}

installwidevine() { \
	mkdir -p /tmp/chromium_widevine/
	cd /tmp/chromium_widevine/
	wget -c https://dl.google.com/linux/deb/pool/main/g/google-chrome-stable/google-chrome-stable_79.0.3945.117-1_amd64.deb
	ar x google-chrome-stable_79.0.3945.117-1_amd64.deb
	tar -xf data.tar.xz
	sudo mv opt/google/chrome/WidevineCdm/ /usr/lib/chromium
	cd /home/$USER/
	rm -rf /tmp/chromium_widevine/
}


installvscodium() { \
	cd /home/"$USER"/.programs
	sudo wget --output-document=codium.deb `curl -s https://api.github.com/repos/VScodium/vscodium/releases | grep browser_download_url | grep 'armhf.deb' | grep -v sha256 | head -n 1 | cut -d '"' -f 4`
	sudo dpkg -i codium.deb
	rm -f codium.deb
	codium --install-extension visualstudioexptteam.vscodeintellicode 
	codium --install-extension eamodio.gitlens 
	codium --install-extension vscjava.vscode-java-pack 
	codium --install-extension siddharthkp.codesandbox-black
	codium --install-extension pkief.material-icon-theme
	rm -rf /home/"$USER"/.config/VSCodium/User/settings.json
	touch /home/"$USER"/.config/VSCodium/User/settings.json
	cat /home/"$USER"/.programs/codium/settings.json > /home/"$USER"/.config/VSCodium/User/settings.json
	clear
}

installwpilib() { \
	mkdir -p /home/"$USER"/wpilib/2020
	cd /home/"$USER"/wpilib/2020
	wget --output-document=WPILib_Linux.tar.gz `curl -s https://api.github.com/repos/wpilibsuite/allwpilib/releases | grep browser_download_url | grep 'Linux' | head -n 1 | cut -d '"' -f 4`
	tar -xf WPILib_Linux.tar.gz
	rm -rf WPILib_Linux.tar.gz
	cd /home/"$USER"/wpilib/2020/vsCodeExtensions
	codium --install-extension JavaDebug.vsix 
	codium --install-extension JavaDeps.vsix
	codium --install-extension JavaLang.vsix
	codium --install-extension WPILib.vsix
	cd /home/"$USER"/wpilib/2020/tools
	java -jar ToolsUpdater.jar 
	touch /home/$USER/.local/share/applications/pathweaver.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC PathWeaver
Exec=java -jar /home/$USER/wpilib/2020/tools/PathWeaver.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/pathweaver.desktop
	touch /home/$USER/.local/share/applications/shuffleboard.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC ShuffleBoard
Exec=java -jar /home/$USER/wpilib/2020/tools/shuffleboard.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/shuffleboard.desktop
	touch /home/$USER/.local/share/applications/outlineviewer.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC OutlineViewer
Exec=java -jar /home/$USER/wpilib/2020/tools/OutlineViewer.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/outlineviewer.desktop
	touch /home/$USER/.local/share/applications/robotbuilder.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC RobotBuilder
Exec=java -jar /home/$USER/wpilib/2020/tools/RobotBuilder.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/robotbuilder.desktop
	touch /home/$USER/.local/share/applications/smartdashboard.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC SmartDashboard
Exec=java -jar /home/$USER/wpilib/2020/tools/SmartDashboard.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/smartdashboard.desktop
	touch /home/$USER/.local/share/applications/toolsupdater.desktop
	echo "[Desktop Entry]
Version=2020
Encoding=UTF-8
Name=FRC ToolsUpdater
Exec=java -jar /home/$USER/wpilib/2020/tools/ToolsUpdater.jar
Terminal=false
Icon=/home/$USER/.programs/wpilib/icon.png
Type=Application
Categories=Application;FRC;WPILib;" > /home/$USER/.local/share/applications/toolsupdater.desktop
clear
}	

# Things that are executed (in order)
installprograms
exitmsg
pause "Press [Return] key to continue..."
clear