#!/bin/bash

sudo apt install gnome-shell-extension-no-annoyance gnome-shell-extension-dash-to-panel gnome-shell-extensions gnome-menus gir1.2-gmenu-3.0 
sudo wget -O /usr/local/bin/gnomeshell-extension-manage "https://raw.githubusercontent.com/NicolasBernaerts/ubuntu-scripts/master/ubuntugnome/gnomeshell-extension-manage"	
sudo chmod +x /usr/local/bin/gnomeshell-extension-manage
gnomeshell-extension-manage --install --extension-id 3628
gnomeshell-extension-manage --install --extension-id 1180
