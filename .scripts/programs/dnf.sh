#!/bin/sh
# autoinstall script for Fedora 34 Workstation - DNF Packages

# check if root and on fedora 34

if hostnamectl | grep "Fedora 34" >/dev/null 2>&1 ; then

### REPOS ###
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-34.noarch.rpm
sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-34.noarch.rpm

### RPM PACKAGES ###

sudo dnf install -y \
ImageMagick \
autoconf \
cabextract \
curl \
dconf-editor \
dnf-plugins-core \
ffmpeg \
flatpak \
fontconfig \
fuse-exfat \
gcc \
gcc-c++ \
gimp \
git \
glances \
gnome-extensions-app \
gnome-tweak-tool \
hplip \
htop \
java-1.8.0-openjdk \
java-1.8.0-openjdk-devel \
java-11-openjdk \
java-11-openjdk-devel \
java-latest-openjdk \
java-latest-openjdk-devel \
libvirt-daemon-kvm \
make \
mono-complete \
net-tools \
network-manager-applet \
nmap \
ntfs-3g \
ntfs-3g-devel \
ntfs-3g-system-compression \
openssh-askpass \
playonlinux \
telnet \
testdisk \
thunderbird \
tlp \
tlp-rdw \
tmux \
transmission \
vim \
virt-manager \
vlc \
winetricks \
wireshark \
xclip \
xinput \
xorg-x11-font-utils \
xrdp \ 
xsel \
yubikey-manager

sudo systemctl enable tlp.service

### RPMS NOT FOUND IN REPOS ###
sudo dnf install -y \
https://code-industry.net/public/master-pdf-editor-5.7.53-qt5_included.x86_64.rpm \
https://github.com/angryip/ipscan/releases/download/3.7.6/ipscan-3.7.6-1.x86_64.rpm

### UPDATE SYSTEM ###
sudo dnf update -y

### REMOVAL AND DEBLOAT ###
sudo dnf remove -y \
abrt-desktop \
evolution \
gnome-boxes \
gnome-calendar \
gnome-characters \
gnome-contacts \
gnome-maps \
gnome-tour \
gnome-weather \
rhythmbox \
totem

else
echo "Please install on a Fedora 34 system."
fi
