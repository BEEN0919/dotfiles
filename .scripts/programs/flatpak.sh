#!/bin/sh
sudo dnf install -y flatpak
# flatpak program list - add or remove your flatpak packages here
sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
sudo flatpak install flathub com.adobe.Flash-Player-Projector -y
sudo flatpak install flathub com.belmoussaoui.Decoder -y
sudo flatpak install flathub com.github.micahflee.torbrowser-launcher -y
sudo flatpak install flathub com.github.tchx84.Flatseal -y
sudo flatpak install flathub com.leinardi.gst -y
sudo flatpak install flathub com.discordapp.Discord -y
sudo flatpak install flathub org.onlyoffice.desktopeditors -y
sudo flatpak install flathub com.spotify.Client -y
sudo flatpak install flathub com.visualstudio.code -y
sudo flatpak install flathub com.github.iwalton3.jellyfin-media-player -y
