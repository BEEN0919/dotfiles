#!/bin/bash
echo "deb http://deb.debian.org/debian/ buster main contrib non-free
deb-src http://deb.debian.org/debian/ buster main contrib non-free
deb http://security.debian.org/debian-security buster/updates main contrib non-free
deb-src http://security.debian.org/debian-security buster/updates main contrib non-free
deb http://ftp.debian.org/debian/ buster-backports main contrib non-free
deb-src http://ftp.debian.org/debian/ buster-backports main contrib non-free
deb http://deb.debian.org/debian/ buster-updates main contrib non-free
deb-src http://deb.debian.org/debian/ buster-updates main contrib non-free" | sudo tee /etc/apt/sources.list
sudo dpkg --add-architecture i386
sudo apt update	
sudo apt install -y curl vlc yubikey-manager snapd dconf-editor gettext libgettextpo-dev libminizip1 bash-completion libre2-5 software-properties-common gparted binutils unzip mlocate glances neofetch libreoffice htop gimp net-tools gnome-tweaks exfat-utils exfat-fuse openjdk-11-jre openjdk-11-jdk tlp tlp-rdw tp-smapi-dkms smartmontools python3-dev python3-pip python3-setuptools
sudo pip3 install thefuck
sudo apt -y autoremove
sudo apt install --fix-broken 
sudo snap install discord spotify
sudo snap connect discord:system-observe
