#!/bin/bash

sudo apt-get install firmware-linux firmware-linux-nonfree libdrm-amdgpu1 xserver-xorg-video-amdgpu mesa-opencl-icd mesa-vulkan-drivers libvulkan1 vulkan-tools vulkan-utils vulkan-validationlayers radeontop -y
sudo apt-get install firmware-iwlwifi -y