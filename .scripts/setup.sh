#!/bin/bash
initsetup(){
	installprerequisites
}

installprerequisites(){
	sudo apt install git dialog -y -q=3 >/dev/null 2>&1
	sudo dnf install git dialog -y >/dev/null 2>&1
	sudo rm -rf /home/"$USER"/.git
	welcomemsg
}

welcomemsg(){
	dialog --title Welcome! --msgbox "This script can install / edit the following:
- Set Git username / email
- Install BEEN0919's Dotfiles
- Install recommended packages
- Install VSCodium with recommended extensions
- Install WPI / FRC Utilities
- Set PC's hostname" 22 76
	selectoptions
}

selectoptions(){
	cmd=(dialog --separate-output --checklist "Select options:" 22 76 16)
	options=(1 "Set up Git username and email" off    # any option can be set to default to "on"
			2 "Install BEEN0919's Custom Settings (dotfiles)" on
			3 "Set hostname of your PC" off)
	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
	clear
	for choice in $choices
	do
		case $choice in
			1)
				gitconfig
				;;
			2)
				clonedotfiles
				;;
			3)
				sethostname
				;;
		esac
	done
}

gitconfig() { \
	gitusername=$(\
  		dialog --title "Configure Git" \
         --inputbox "Enter your full name:" 8 40 \
  		3>&1 1>&2 2>&3 3>&- \
		)
	gitemail=$(\
  		dialog --title "Configure Git" \
         --inputbox "Enter your email:" 8 40 \
  		3>&1 1>&2 2>&3 3>&- \
		)
	git config --global user.name "$gitusername"
	git config --global user.email "$gitemail"
	git config --global credential.helper store
}

clonedotfiles() { \
		rm -rf /home/'$USER'/.config /home/'$USER'/.scripts /home/'$USER'/.programs /home/'$USER'/.papes /home/'$USER'/.vscode
		git clone https://gitlab.com/soconnor0919/dotfiles.git /home/$USER/dotfiles --progress 2>&1 | dialog --title "Cloning Dotfiles" --progressbox 30 120
		mv -f /home/"$USER"/dotfiles/* /home/"$USER"/dotfiles/.* /home/"$USER"/
		cd /home/"$USER"/
		rm -rf dotfiles/
}


sethostname() { \
	hostname=$(\
  		dialog --title "Choose Hostname" \
         --inputbox "Enter your new hostname:" 8 40 \
  		3>&1 1>&2 2>&3 3>&- \
		)# clear
    echo "done = good;"
    echo "The installer has finished."
    echo
	sudo hostnamectl set-hostname $hostname
}
selectdistro() { \
	cmd=(dialog --separate-output --checklist "Select your distro:" 22 76 16)
	options=(1 "Debian" off    # any option can be set to default to "on"
			2 "Ubuntu or one of its derivatives" off
			3 "Fedora or one of its spins" off
			4 "Other" off)
	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
	clear
	for choice in $choices
	do
		case $choice in
			1)
				wget -O debian.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/distros/debian.sh
				bash debian.sh
				break
            	;;
			2)
				wget -O ubuntu.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/distros/ubuntu.sh
				bash ubuntu.sh
				break
            	;;
			3)
				wget -O fedora.sh https://gitlab.com/soconnor0919/dotfiles/-/raw/master/.scripts/distros/fedora.sh
				bash fedora.sh
				break
				;;
			4)
				echo "I'm sorry, this script doesn't support your distro yet."
				break
				exitmsg
            	;;

		esac
	done

}

deletus() { \
	cd ~
	sudo rm -r soconnor0919dotfiles >/dev/null 2>&1
	sudo rm -r setup.sh >/dev/null 2>&1
	sudo rm -r debian.sh >/dev/null 2>&1
	sudo rm -r ubuntu.sh >/dev/null 2>&1
	sudo rm -r fedora.sh >/dev/null 2>&1
	sudo rm -r apt.sh >/dev/null 2>&1
	sudo rm -rf /home/"$USER"/.git > /dev/null 2>&1
	sudo rm -rf /home/"$USER"/LICENSE > /dev/null 2>&1
	sudo rm -rf /home/"$USER"/CONTRIBUTING.md > /dev/null 2>&1
}

exitmsg() { \
	dialog --title Finished! --msgbox "The installer has finished. Press OK to exit." 22 76
	clear
}

# Things that are executed (in order)
initsetup
selectdistro
exitmsg
deletus